
import yaml
from yaml.composer import Composer
from yaml.constructor import Constructor
import argparse
import re

from typing import Generator, Dict, List, Any, Optional, IO

import pprint

import client
import client_output

import env
from env import Environment

def bake() -> client_output.BakeForResult:
    return client.bake(
        'bootstrap1',
        [
            '--max-priority',
            '512',
            '--minimal-timestamp',
            '--minimal-fees',
            '0',
            '--minimal-nanotez-per-byte',
            '0',
            '--minimal-nanotez-per-gas-unit',
            '0',
        ],
    )


class FailedSpecificationError(Exception):
    """Raised when client output couldn't be parsed."""

    def __init__(self, message: str):
        super().__init__()
        self.message = message

class InvalidSpecificationStep(Exception):
    """Raised when client output couldn't be parsed."""

    def __init__(self, name: str, step: dict, message: str):
        super().__init__()
        self.name = name
        self.step = step
        self.message = message

def assert_obj_eq(obj1, obj2, ignore: List[str] = None):
    for (key, val) in obj1.items():
        if ignore is not None and key in ignore:
            continue

        try:
            assert key in obj2
        except AssertionError as exc:
            raise FailedSpecificationError(f"""
            Could not find key {key} in\n{pprint.pformat(obj2)} when
            comparing with\n{pprint.pformat(obj1)}
            """) from exc

        try:
            assert obj2[key] == val
            print(f"   - [X] comparing {obj2[key]} with {val}")
        except AssertionError as exc:
            raise FailedSpecificationError(f"""
            Different value under key `{key}`,
            {val} != {obj2[key]} when comparing\n
            {pprint.pformat(obj1)} with\n {pprint.pformat(obj2)}
            """) from exc


RE_VAR = re.compile(r"\$(\w+(\.\w+)*)(?=($|\W))")
def interpolate(env: Environment, obj: Any):
    if not isinstance(obj, str):
        return obj

    def replace(match):
        var = match.group(1)
        return env.lookup(var, obj)

    return re.sub(RE_VAR, replace, obj)

def interpolate_dict(env: Environment, obj: dict, ignore=None):
    for key, val in obj.items():
        if ignore is not None and key in ignore:
            continue
        obj[key] = interpolate(env, val, )
    return obj

def template_expansion(templates: dict, attrs: dict):
    template = attrs.pop('_extends', None)
    if template is None:
        return attrs
    assert template in templates
    template_inst = templates[template].copy()
    template_inst = template_expansion(templates, template_inst)
    template_inst.update(attrs)
    return template_inst

def template_expansion_rec(templates, obj: dict):
    obj = template_expansion(templates, obj)
    for (key, val) in obj.items():
        if isinstance(val, dict):
            obj[key] = template_expansion_rec(templates, val)
    return obj


def step_gen_keys(env: Environment, attrs: dict):
    alias = interpolate(env, attrs['alias'])
    client.gen_keys(alias)
    output = client.show_address(alias, show_secret = True)
    env.set([alias, 'hash'], output.hash)
    env.set([alias, 'public_key'], output.public_key)
    env.set([alias, 'secret_key'], output.secret_key)

def step_run_script(env: Environment, attrs: dict):
    # attrs = template_expansion(templates, attrs)
    output = client.run_script(
        interpolate(env, attrs['contract']),
        interpolate(env, attrs['storage']),
        interpolate(env, attrs['parameter']),
        interpolate(env, attrs['entrypoint']),
        amount=interpolate(env, attrs['amount']) if 'amount' in attrs else None,
        source=interpolate(env, attrs['source']) if 'source' in attrs else None,
        sender=interpolate(env, attrs['sender']) if 'sender' in attrs else None
    )

    assert not ('expect_failure' in attrs and 'expect_success' in attrs)

    if 'expect_success' in attrs:
        expected_attributes = attrs['expect_success']
        assert output.client_return_code == 0
        assert output.storage == expected_attributes['storage']
        if 'emitted_operations' in expected_attributes:
            assert len(expected_attributes['emitted_operations']) == \
                len(output.internal_operations)
            for (expected_op, operation) in zip(expected_attributes['emitted_operations'],
                                                output.internal_operations):
                expected_op = interpolate_dict(env, expected_op, ignore=['__line__'])
                assert_obj_eq(expected_op, operation, ignore=['__line__'])

        if 'big_map_diff' in expected_attributes:

            if len(expected_attributes['big_map_diff']) != len(output.big_map_diff):
                raise FailedSpecificationError(f"""
Expected big_map_diff with {len(expected_attributes['big_map_diff'])}
elements, got {len(output.big_map_diff)}.

Expected:
{pprint.pformat(expected_attributes['big_map_diff'])}
Got:
{pprint.pformat(output.big_map_diff)}.
""")

            for (expected_diff, diff) in zip(expected_attributes['big_map_diff'],
                                         output.big_map_diff):
                expected_diff = interpolate_dict(env, expected_diff, ignore=['__line__', 'type'])
                assert_obj_eq(expected_diff, diff, ignore=['__line__'])
    elif 'expect_failure' in attrs:
        expected_attributes = attrs['expect_failure']
        assert output.client_return_code == 1
        assert_obj_eq(
            expected_attributes, {
                'failure_line': output.failure_line,
                'failure_columns': output.failure_columns,
                'failure_type': output.failure_type,
                'failure_with': output.failure_with
            }, ignore=['__line__']
        )

def step_originate_contract(env: Environment, attrs: dict):
    # output =
    contract_name = interpolate(env, attrs['contract_name'])
    output = client.originate(
        contract_name,
        interpolate(env, attrs['amount']),
        interpolate(env, attrs['from']),
        interpolate(env, attrs['running']),
        ['--init', interpolate(env, attrs['init_storage'])] +
        ['--burn-cap', str(attrs['burn_cap'])]
    )
    env.set([contract_name, 'hash'], output.contract)
    bake()


def step_type_check_data(env: Environment, attrs: dict):
    data = interpolate(env, attrs['data'])
    typ = interpolate(env, attrs['type'])
    (_output, _output_err, output_code) = client.typecheck_data(data, typ)

    expect_success = attrs.get('expect_success', True)
    if expect_success and output_code != 0:
        raise FailedSpecificationError(f"Expected {data} to typecheck as {typ}")
    if not expect_success and output_code == 0:
        raise FailedSpecificationError(f"Did not expect {data} to typecheck as {typ}")


STEPS = {
    'run_script': step_run_script,
    'originate_contract': step_originate_contract,
    'gen_keys': step_gen_keys,
    'typecheck_data': step_type_check_data
}

def run_step(env, name, step):
    print(f" - {name}")

    if len(step.items()) != 2: # counting __line__
        raise InvalidSpecificationStep(name, step, "Step instances should have exactly one key")

    _line = step.pop('__line__')
    (step_name, step_attrs) = step.popitem()

    if step_name in STEPS:
        STEPS[step_name](env, step_attrs)
    else:
        steps = ", ".join(STEPS.keys())
        print(f" NOK: not step definition for {step_name}. Available steps:")
        print(f"      {steps}")
        assert False

def run_scenario(env, scenario_name, scenario):
    print(f"* {scenario_name}")
    for name, step in scenario.items():
        if name == '__line__':
            continue
        run_step(env, name, step)

def run_suite(suite):
    env = Environment()
    for key, val in suite.get('constants', {}).items():
        if key == '__line__':
            continue
        env.set([key], val, mod='CONSTANT')

    templates = suite.get('templates', {})
    suite['scenarios'] = template_expansion_rec(templates, suite['scenarios'])

    for name, scenario in suite['scenarios'].items():
        if name == '__line__':
            continue
        fresh_env = env.copy()
        run_scenario(fresh_env, name, scenario)


def yaml_load_with_location(data_file: IO[Any]) -> Any:
    loader = yaml.Loader(data_file.read())
    def compose_node(parent, index):
        # the line number where the previous token has ended (plus empty lines)
        line = loader.line
        node = Composer.compose_node(loader, parent, index)
        node.__line__ = line + 1
        return node
    def construct_mapping(node, deep=False):
        mapping = Constructor.construct_mapping(loader, node, deep=deep)
        mapping['__line__'] = node.__line__
        return mapping
    loader.compose_node = compose_node # type: ignore
    loader.construct_mapping = construct_mapping # type: ignore
    return loader.get_single_data()


def main():
    parser = argparse.ArgumentParser(description="""
Acceptance testing for Michelson scripts.
    """)
    parser.add_argument('spec_file',
                        metavar='spec.yaml',
                        type=argparse.FileType('r'),
                        help='the data file')
    args = parser.parse_args()
    with args.spec_file as spec_file:
        data = yaml_load_with_location(spec_file)
        try:
            run_suite(data)

        # Something is wrong in the specification file
        except InvalidSpecificationStep as exc:
            print(f"Error when executing step {exc.name}: {exc.message}" +
                  "Step attributes:\n" + pprint.pformat(exc.step))
        except env.UndefinedPath as exc:
            print(f"Could not find `{exc.var}` in environment:\n" +
                  pprint.pformat(exc.env) +
                  f"\nwhen interpolating string `{exc.string}`")
        except env.SetConstant as exc:
            print(f"Attempting to overwrite constant {exc.var} with value\n" +
                  pprint.pformat(exc.val))

        # Something is wrong with the system under test
        except FailedSpecificationError as exc:
            print("FailedSpecificationError")
            print(exc.message)

        # Something has gone wrong with client interactoin
        except client_output.InvalidClientOutput as exc:
            print("InvalidClientOutput")
            print(exc.client_output)
            print(exc.client_output_err)
            raise exc

if __name__ == '__main__':
    main()
