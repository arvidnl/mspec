import os
import subprocess
import sys

from typing import Any, List, Optional, Tuple

import client_output

CLIENT='tezos-client'

def format_command(cmd: List[str]) -> str:
    # TODO the displayed command may not be 'shell' ready, for instance
    # Michelson string parameters may requires additional quotes
    color_code = '\033[34m'
    endc = '\033[0m'
    cmd_str = " ".join(cmd)
    return f'{color_code}# {cmd_str}{endc}'

def run_generic(
        params: List[str],
        check: bool = False,
        stdin: str = "",
        verbose: bool = True,
) -> Tuple[str, str, int]:
    """Run an arbitrary command

    Args:
        params (list): list of parameters given to the tezos-client,
        check (bool): raises an exception if client call fails
        stdin (string): string that will be passed as standard
                        input to the process
    Returns:
        (stdout of command, stderr of command, return code)

    The actual command will be displayed according to 'format_command'.
    Client output (stdout, stderr) will be displayed unprocessed.
    Fails with `CalledProcessError` if command fails
    """
    client = [CLIENT, '--wait', 'none']
    cmd = client + params

    if verbose:
        print(format_command(cmd))

    new_env = os.environ.copy()

    # if self._disable_disclaimer:
    #     new_env["TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER"] = "Y"

    process = subprocess.Popen(
        cmd,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        env=new_env,
    )
    outstream, errstream = process.communicate(input=stdin.encode())
    stdout = outstream.decode('utf-8')
    stderr = errstream.decode('utf-8')
    # if stdout:
    #     print(stdout)
    # if stderr:
    #     print(stderr, file=sys.stderr)
    if check:
        if process.returncode != 0:
            raise subprocess.CalledProcessError(
                process.returncode, cmd, stdout, stderr
            )
    # `+ ""` makes pylint happy. It can't infer stdout/stderr can't
    # be `None` thanks to the `capture_output=True` option.
    return (stdout + "", stderr + "", process.returncode)


def gen_keys(alias: str, args: List[str] = None) -> Tuple[str, str, int]:
    cmd = ['gen', 'keys', alias]
    if args is None:
        args = []
        cmd += args
    return run_generic(cmd)


def show_address(
        name: str, show_secret: bool = False
) -> client_output.ShowAddressResult:
    cmd = ['show', 'address', name]
    if show_secret:
        cmd += ['--show-secret']
    return client_output.ShowAddressResult(*run_generic(cmd))


# def run(
#         params: List[str],
#         check: bool = True,
# ) -> str:
#     """Like 'run_generic' but returns just stdout."""
#     (stdout, _, _) = run_generic(params, check)
#     return stdout

def run_script(
        contract: str,
        storage: str,
        inp: str,
        entrypoint: str = None,
        amount: float = None,
        source: str = None,
        sender: str = None,
        balance: float = None,
        trace_stack: bool = False,
        gas: int = None,
        file: bool = True,
) -> client_output.RunScriptResult:
    if file:
        assert os.path.isfile(contract), f'{contract} is not a file'
    cmd = [
        'run',
        'script',
        contract,
        'on',
        'storage',
        storage,
        'and',
        'input',
        inp,
    ]
    if entrypoint is not None:
        cmd += ['--entrypoint', entrypoint]
    if amount is not None:
        cmd += ['--amount', '%.6f' % amount]
    if balance is not None:
        cmd += ['--balance', '%.6f' % balance]
    if trace_stack:
        cmd += ['--trace-stack']
    if gas is not None:
        cmd += ['--gas', '%d' % gas]
    # yes, this is confusing
    if source is not None:
        cmd += ['--payer', source]
    # yes, this is confusing
    if sender is not None:
        cmd += ['--source', sender]
    return client_output.RunScriptResult(*run_generic(cmd))

def originate(
    contract_name: str,
    amount: float,
    sender: str,
    contract: str,
    args: List[str] = None,
) -> client_output.OriginationResult:
    cmd = [
        'originate',
        'contract',
        contract_name,
        'transferring',
        str(amount),
        'from',
        sender,
        'running',
        contract,
        '--force'
    ]
    if args is None:
        args = []
    cmd += args
    return client_output.OriginationResult(*run_generic(cmd))

def bake(
    account: str, args: List[str] = None
) -> client_output.BakeForResult:
    cmd = ['bake', 'for', account]
    if args is None:
        args = []
    cmd += args
    return client_output.BakeForResult(*run_generic(cmd))


def typecheck_data(
        data: str, typ: str, legacy=False
) -> Tuple[str, str, int]:
    params = ['typecheck', 'data', data, 'against', 'type', typ]
    if legacy:
        params += ['--legacy']
    return run_generic(params)
