"""Structured representation of client output."""
import json
import re
from enum import auto, Enum, unique
from typing import List, Dict

# TODO This is incomplete. Add additional attributes and result classes as
#      they are needed


class InvalidClientOutput(Exception):
    """Raised when client output couldn't be parsed."""

    def __init__(self, client_output: str, client_output_err: str):
        super().__init__(self)
        self.client_output = client_output
        self.client_output_err = client_output_err


class InvalidExitCode(Exception):
    """Raised when client existed with unexpected exit code."""

    def __init__(self, exit_code: int):
        super().__init__(self)
        self.exit_code = exit_code


class Result:
    def __init__(self, client_output: str,
                 client_output_err: str,
                 client_return_code: int):
        self.client_output = client_output
        self.client_output_err = client_output_err
        self.client_return_code = client_return_code

class BakeForResult(Result):
    """Result of a 'baker for' operation."""

    def __init__(self, client_output: str, client_output_err: str, client_return_code: int):
        super().__init__(client_output, client_output_err, client_return_code)
        pattern = r"Injected block ?(\w*)"
        match = re.search(pattern, client_output)
        if match is None:
            raise InvalidClientOutput(client_output, client_output_err)
        self.block_hash = match.groups()[0]

class RunScriptResult(Result):
    """Result of a 'get script' operation."""

    SUCCESS_PATTERN_STR = r"(?s)storage\n\s*(.*)\nemitted operations\n"
    FAILURE_PATTERN_STR = r"(?s)Runtime error in contract (.*)\n"

    def __init__(self, client_output: str, client_output_err: str, client_return_code: int):
        super().__init__(client_output, client_output_err, client_return_code)

        if re.search(self.SUCCESS_PATTERN_STR, client_output) is not None:
            self.parse_success(client_output, client_output_err)
        elif re.search(self.FAILURE_PATTERN_STR, client_output_err) is not None:
            self.parse_failure(client_output, client_output_err)
        else:
            raise InvalidClientOutput(client_output, client_output_err)

    def parse_failure(self, client_output, client_output_err):
        match = re.search(self.FAILURE_PATTERN_STR, client_output_err)
        self.failure_in = match.group(0)

        message_pattern = re.compile(
            r"At line (\d+) characters (\d+) to (\d+),\n" +
            r"script reached (FAILWITH|FAIL)? instruction\n" +
            r"with (.*)"
        )
        match = re.search(message_pattern, client_output_err)
        if not match:
            raise InvalidClientOutput(client_output, client_output_err)

        self.failure_line = match.group(1)
        self.failure_columns = (match.group(2), match.group(3))
        self.failure_type = match.group(4)
        self.failure_with = match.group(5)

    def parse_success(self, client_output, client_output_err):
        # read storage output
        match = re.search(self.SUCCESS_PATTERN_STR, client_output)
        if match is None:
            raise InvalidClientOutput(client_output, client_output_err)
        self.storage = match.groups()[0]

        # read operation output
        self.internal_operations = None
        pattern = r"(?s)emitted operations\n\s*(.*)\nbig_map diff"
        match = re.search(pattern, client_output)
        if match is not None:
            # Amount: ꜩ0.05
            # From: KT1BEqzn5Wx8uJrZNvuS9DVHmLvG9td3fDLi
            # To: KT1CrAC8RqhMdZmg2Jz4WqGoabkPLSNEYPN8
            # Parameter: 1
            compiled_pattern = re.compile(
                r"Amount: ꜩ(.*)\n\s+" +
                r"From: (.*)\n\s+" +
                r"To: (.*)\n\s+" +
                r"Parameter: (.*)"
            )
            self.internal_operations = []
            for match_transaction in compiled_pattern.finditer(match.group(1)):
                self.internal_operations.append({
                    'amount': float(match_transaction.group(1)),
                    'from': match_transaction.group(2),
                    'to': match_transaction.group(3),
                    'parameter': match_transaction.group(4),
                })

        # read map diff output
        self.big_map_diff = []  # type: List
        pattern_str = r"big_map diff\n"
        match = re.search(pattern_str, client_output)
        if match is not None:
            compiled_pattern = re.compile(r"  ((New|Set|Del|Unset).*?)\n")
            for match_diff in compiled_pattern.finditer(
                client_output, match.end(0)
            ):
                typ = match_diff.group(2)
                action = match_diff.group(1)
                if typ == "New":
                    #  New map(0) of type (big_map address (pair (nat :balance) (map :approvals address nat)))
                    action_pattern = re.compile(r"New map\((\d+)\) of type (.*)")
                    match = re.match(action_pattern, action)
                    self.big_map_diff.append({
                        'type': 'new_map',
                        'index': int(match.group(1)),
                        'map_type': match.group(2)
                    })
                elif typ == "Set":
                    #  Set map(0)["tz1dVrPrdWa1dSXxpQGoMTdPab4oWKcPQK3A"] to (Pair 1 {})
                    action_pattern = re.compile(r"Set map\((\d+)\)\[(.*)\] to (.*)")
                    match = re.match(action_pattern, action)
                    self.big_map_diff.append({
                        'type': 'set_map',
                        'index': int(match.group(1)),
                        'key': match.group(2),
                        'value': match.group(3)
                    })
                elif typ == "Del":
                    assert False
                elif typ == "Unset":
                    assert False
                else:
                    assert False

                # self.big_map_diff.append([match_diff.group(1)])

        self.client_output = client_output




class OriginationResult(Result):
    """Result of an 'originate contract' operation."""

    def __init__(self, client_output: str, client_output_err: str, client_return_code: int):
        super().__init__(client_output, client_output_err, client_return_code)

        pattern = r"New contract ?(\w*) originated"
        match = re.search(pattern, client_output)
        if match is None:
            raise InvalidClientOutput(client_output, client_output_err)
        self.contract = match.groups()[0]
        pattern = r"Operation hash is '?(\w*)"
        match = re.search(pattern, client_output)
        if match is None:
            raise InvalidClientOutput
        self.operation_hash = match.groups()[0]


class ShowAddressResult(Result):
    """Result of a 'show address' command."""

    def __init__(self, client_output: str, client_output_err: str, client_return_code: int):
        super().__init__(client_output, client_output_err, client_return_code)

        pattern = r"Hash: ?(\w+)"
        match = re.search(pattern, client_output)
        if match is None:
            raise InvalidClientOutput(client_output, client_output_err)
        self.hash = match.groups()[0]
        pattern = r"Public Key: ?(\w+)"
        match = re.search(pattern, client_output)
        if match is None:
            self.public_key = None
        else:
            self.public_key = match.groups()[0]
            pattern = r"Secret Key: ?(\w+:\w+)"
            match = re.search(pattern, client_output)
        if match is None:
            self.secret_key = None
        else:
            self.secret_key = match.groups()[0]
