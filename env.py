from typing import Any, List

class UndefinedPath(Exception):
    def __init__(self, var: str, env: dict, string: str):
        super().__init__()
        self.var = var
        self.env = env
        self.string = string

class SetConstant(Exception):
    def __init__(self, var: str, val: dict):
        super().__init__()
        self.var = var
        self.val = val

class Environment:
    def __init__(self, init: dict = None):
        self.env = {} if init is None else init

    def lookup(self, var: str, obj: dict):
        def lookup_aux(env1, path):
            assert len(path) > 0
            name = path[0]
            if env1 is None or name not in env1:
                raise UndefinedPath(var, self.env, obj)
            if len(path) == 1:
                return env1[name]['val']
            return lookup_aux(env1[name]['val'], path[1:])
        return lookup_aux(self.env, var.split('.'))

    def set(self, path: List[str], val: Any, mod: str = None):
        def set_aux(env1, path):
            assert len(path) > 0
            name = path[0]
            if name not in env1:
                env1[name] = {'mod': None, 'val': None}

            if len(path) == 1:
                if env1[name]['mod'] == 'CONSTANT':
                    raise SetConstant(name, val)
                env1[name] = {'val': val, 'mod': mod}
            else:
                if env1[name]['val'] is None:
                    env1[name]['val'] = {}
                set_aux(env1[name]['val'], path[1:])
        set_aux(self.env, path)

    def copy(self):
        return Environment(self.env.copy())
